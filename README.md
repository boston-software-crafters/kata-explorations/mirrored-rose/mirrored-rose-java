# Mirrored Rose

## Introduction

Hi and welcome to team Gilded Rose. As you know, we 
are a small inn with a prime location in a prominent 
city ran by a friendly innkeeper named Allison. We 
frequently accept magic items from adventurers in
trade; going forward, we will be reselling those
items in our shop.

When we accept an item in trade, our quartermaster
evaluates the item to determine the quality of
the item and how long we should keep it on our
shelves.  This information is entered into the
our legacy inventory system.

Unfortunately, our goods are constantly degrading 
in quality as they approach their sell by date.

We need you to develop an extension to the legacy
system, that computes new sellIn and quality
values for items each night, so that we can
haggle from a position of strength.

Here are some examples of different magic items
that appear in our inventory

```               
+5 Dexterity Vest
Aged Brie
Elixir of the Mongoose
Sulfuras, Hand of Ragnaros
Backstage passes to a TAFKAL80ETC concert
Conjured Mana Cake
```

## Getting Started

Download a copy of this project from
https://gitlab.com/boston-software-crafters/kata-starter-projects/mirrored-rose/mirrored-rose-java

Load the project into your preferred IDE.

Within`src/test/java/release/` you will find eight packages; each package
contains a suite of `ExampleTests`, convering specific inputs that may
discover coding errors, an and exhaustive `AcceptanceTests` suite, to
verify that your solution handles all inputs correctly.

The test suites are simply java programs; invoke the main method to run
the tests contained within.

In the case that a test should fail, you will observe a single error
like what follows below.

``` 
At the end of each day our system lowers both sellIn and quality for every item
	Scenario: +5 Dexterity Vest, 10, 20
	Expected: +5 Dexterity Vest, 9, 19
	  Actual: +5 Dexterity Vest, 10, 20
FAILED
```

Each release package also includes a reference implementation, which you 
may consult, but be warned: by design, the references implementations
are not clear.

## The exercise

In this exercise, we are implementing a component of an inventory management
system for a merchant in an on-line fantasy world.  Items kept in inventory
age, the aging process impacts the quality of the item, according to rules
described below.

The exercise is divided into eight "releases"; in each release, we will
be modifying the code in `inventory.Inventory.updateQuality` to incorporate
new business rules.

(Not all of the rules are backwards compatible with earlier rules - it
is, after all, the nature of requirements to change.  In case of conflict,
later rules take precedence over the earlier rules.)

Each `ExampleTests` suite includes some checks for the new rules, and
a number of "regression tests" to evaluate the earlier rules that should
still hold.

For each release you should
 * update your implementation such that `updateQuality` has the
required behaviors (use TDD and the provided ExampleTests to ensure
that your implementation is correct)
 * refactor the code - keep cleaning things up until you are convinced
that no improvements have been deferred and that the code is good enought
to satisfy code review 
 * Verify that the `AcceptanceTests` suite for the release passes
 * Commit your changes.

### General Inventory Policies

* All items have a SellIn value which denotes the
  number of days we have to sell the item
* All items have a Quality value which denotes how
  valuable the item is

### v1 Release

* At the end of each day our system lowers both values
  for every item

### v2 Release

* Once the sell by date has passed (sellIn is zero or lower), 
  Quality degrades twice as fast

### v3 Release

* The Quality of an item is never negative

### v4 Release

* "Aged Brie" actually increases in Quality the older it gets.
This also means it increases twice as fast after the sellIn 
has passed.  

### v5 Release

* The Quality of an item is never more than 50

### v6 release

* "Sulfuras", being a legendary item, never has to
  be sold (sellIn does not change) or decreases in Quality; 
  the quality of Sulfuras may be anywhere between 20 and 100 (inclusive).

### v7 release

* "Backstage passes”, like aged brie, increases in Quality
  as it’s SellIn value approaches; Quality increases by 2 when
  there are 10 days or less and by 3 when there are 5 days or
  less BUT Quality drops to 0 after the concert

### v8 release

* "Conjured" items degrade in Quality twice as fast as
  normal items (both before and after the sellIn date)

## Problem Constraints

The `legacy` package contains classes for the
legacy inventory management system.  These
classes MUST NOT be changed during the exercise.

Each step in the problem introduces a new
inventory policy requirement.  Make necessary
changes to support the new requirement AND all
prior requirements.

You SHOULD NOT move on to the next step in
the problem until you are satisfied that
the current design has _no technical debt_.

> Shipping first time code is like going into debt. A
> little debt speeds development so long as it is paid
> back promptly with a rewrite. Objects make the cost
> of this transaction tolerable. The danger occurs
> when the debt is not repaid. Every minute spent on
> not-quite-right code counts as interest on that debt.
> Entire engineering organizations can be brought to a
> stand-still under the debt load of an unconsolidated
> implementation, object- oriented or otherwise.
> -- [Ward Cunningham, 1992][4]

You SHOULD commit your work before you introduce
the next test.

You MAY create additional classes and files within
the inventory package.

You MAY extend the suite of provided tests with
additional tests.

## Links

This exercise is based on the Gilded Rose exercise.

Terry Hughes created the Gilded Rose exercise in
February 2011; it was documented by [Bobby Johnson][1],
who also contributed an initial [implementation written
in C#][2]

[Emily Bache maintains a repository][3] of ports of
the Gilded Rose exercise to many other languages.

[1]: http://iamnotmyself.com/2011/02/14/refactor-this-the-gilded-rose-kata/
[2]: https://github.com/NotMyself/GildedRose
[3]: https://github.com/emilybache/GildedRose-Refactoring-Kata
[4]: http://c2.com/doc/oopsla92.html
