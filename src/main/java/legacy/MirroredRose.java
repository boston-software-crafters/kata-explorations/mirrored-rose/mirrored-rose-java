package legacy;

import inventory.Inventory;

public class MirroredRose {
    Item[] items;

    public MirroredRose(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        for (Item item : items) {
            Inventory.updateQuality(item);
        }
    }
}
