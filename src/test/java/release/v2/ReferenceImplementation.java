package release.v2;

import legacy.Item;

class ReferenceImplementation {
    public static void updateQuality(Item item) {
        Item[] items = {item};
        legacyUpdate(items);
    }

    static void legacyUpdate(Item... items) {
        for(int i = 0; i < items.length; ++i) {
            items[i].quality = items[i].quality - 1;

            items[i].sellIn = items[i].sellIn - 1;

            if (items[i].sellIn < 0) {
                items[i].quality = items[i].quality - 1;
            }
        }
    }
}
